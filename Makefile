CC=gcc -Wall -O3 -g


LDFLAGS=-lpng

BIN=png_transform

all : $(BIN)

% : %.c
	$(CC) -o $@ $^ $(LDFLAGS)

clean :
	rm -f $(BIN) *.o *~

