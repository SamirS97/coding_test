This file is the report of the debug steps Samir Sayah followed for the coding test for Kalray.


DEBUG STEPS: 
The given program was supposed to apply a transformation to the input image and output the transformed image. However the output image was exactly the same as the input. This was due to an if condition that was not guarding a return statement (l.162), I noticed that the "Starting processing\n" (l.165) was never printed.

Another thing I noticed was that the row pointer was assigned in the inner loop when going through the image. I reorganized the loops and made assigned the row pointer each time we changed a row i.e in the outer for loop (l.168 to l.181).

ANSWERS: 
We can first check that the first transformation is accurate by comparing the images in the folder goldens with the generated ones. We can see that the RMSE of the difference between the images is always 0, they are the same.

The new functionnality has been done by adding 4 parameters, the first one is a boolean (0 or 1) and corresponds to the transformation we want to perform (old or new). The 3 other parameters correspond to the pixel values for each channel. If we specify 0 for the transformation, the pixel parameters will not be used and can be anything.
Example: ./png_manycore-img.png imgs_transformed/manycore-img.png 0 1 1 1     
Here the old transformation is performed and the last 3 parameters are not used.

The correctness of the new functionnality can easily be checked by running the program with the following values for the pixel channels {255;0;0}, {0;255;0}, {0;0;255} and then checking if all the pixels in the the output images are red/green/blue.